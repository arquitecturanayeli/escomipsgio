library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RegistroEstados is
    Port ( CLK : 				in  STD_LOGIC;
           CLR : 				in  STD_LOGIC;
           LF  : 				in  STD_LOGIC;
           BANDERAS_IN  :  in  STD_LOGIC_VECTOR (3 downto 0);
           BANDERAS_OUT :  out  STD_LOGIC_VECTOR (3 downto 0));
end RegistroEstados;

architecture Behavioral of RegistroEstados is

begin

PREGEDO: PROCESS (CLK, CLR)
BEGIN
IF(CLR = '1')THEN
	BANDERAS_OUT <= "0000";
ELSIF(FALLING_EDGE(CLK))THEN
	IF(LF = '1')THEN
		BANDERAS_OUT <= BANDERAS_IN;
	END IF;
END IF;
END PROCESS PREGEDO;
end Behavioral;
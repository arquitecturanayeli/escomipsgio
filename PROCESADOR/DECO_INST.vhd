library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity DECO_INST is
    Port ( OP_CODE : in  STD_LOGIC_VECTOR (4 downto 0);
           TIPOR : out  STD_LOGIC;
           BEQ : out  STD_LOGIC;
           BNE : out  STD_LOGIC;
           BLT : out  STD_LOGIC;
           BLE : out  STD_LOGIC;
           BGT : out  STD_LOGIC;
           BGE : out  STD_LOGIC);
end DECO_INST;

architecture PROGRAMA of DECO_INST is

begin
	TIPOR <= '1' WHEN( OP_CODE = "00000" ) ELSE '0';
	BEQ	<= '1' WHEN( OP_CODE = "01101" ) ELSE '0';
	BNE	<= '1' WHEN( OP_CODE = "01110" ) ELSE '0';
	BLT	<= '1' WHEN( OP_CODE = "01111" ) ELSE '0';
	BLE 	<= '1' WHEN( OP_CODE = "10000" ) ELSE '0';	
	BGT 	<= '1' WHEN( OP_CODE = "10001" ) ELSE '0';	
	BGE 	<= '1' WHEN( OP_CODE = "10010" ) ELSE '0';	
end PROGRAMA;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity CONDICION is
    Port ( RBAND : in  STD_LOGIC_VECTOR (3 downto 0);
           EQ : out  STD_LOGIC;
           NEQ : out  STD_LOGIC;
           L : out  STD_LOGIC;
           LE : out  STD_LOGIC;
           G : out  STD_LOGIC;
           GE : out  STD_LOGIC);
end CONDICION;

architecture PROGRAMA of CONDICION is
begin

	-- RBAND(0) = C
	-- RBAND(1) = Z
	-- RBAND(2) = N
	-- RBAND(3) = OV
	-- OV N Z C
				
	EQ  <= RBAND(1);  --z
	NEQ <= NOT RBAND(1); -- Not z
	G   <= (NOT(RBAND(2) XOR RBAND(3))) AND (NOT RBAND(1));
	GE  <= (NOT(RBAND(2) XOR RBAND(3))) OR RBAND(1);
	L   <= ((RBAND(2) XOR RBAND(3)) AND (NOT(RBAND(1))));
	LE  <= ((RBAND(2) XOR RBAND(3)) OR RBAND(1));
	
end PROGRAMA;


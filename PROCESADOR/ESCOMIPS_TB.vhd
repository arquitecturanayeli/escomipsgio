LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY ESCOMIPS_TB IS
END ESCOMIPS_TB;
 
ARCHITECTURE behavior OF ESCOMIPS_TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT escomips
    PORT(
         CLK : IN  std_logic;
         CLR : IN  std_logic;
			SALALU : OUT std_logic_vector(15 downto 0);
			WD : OUT STD_LOGIC;
         SAL : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal CLR : std_logic := '0';

 	--Outputs
   signal SAL : std_logic_vector(15 downto 0);
	signal SALALU : std_logic_vector(15 downto 0);
	SIGNAL WD : STD_LOGIC;

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: escomips PORT MAP (
          CLK => CLK,
          CLR => CLR,
			 SALALU => SALALU,
			 WD => WD,
          SAL => SAL
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.

		clr <= '1';
      wait UNTIL RISING_EDGE(CLK);
		wait UNTIL RISING_EDGE(CLK);
		
		clr <= '0';
      wait UNTIL RISING_EDGE(CLK);
		wait for CLK_period*100;

      -- insert stimulus here 

      wait;
   end process;

END;

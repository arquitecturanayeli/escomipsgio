LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY o IS
END o;
 
ARCHITECTURE behavior OF o IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT main
    PORT(
         CODIGO_FUNCION : IN  std_logic_vector(3 downto 0);
         CODIGO_OPERACION : IN  std_logic_vector(4 downto 0);
         CLK : IN  std_logic;
         CLR : IN  std_logic;
         BANDERAS : IN  std_logic_vector(3 downto 0);
         LF : IN  std_logic;
         S : OUT  std_logic_vector(19 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CODIGO_FUNCION : std_logic_vector(3 downto 0) := (others => '0');
   signal CODIGO_OPERACION : std_logic_vector(4 downto 0) := (others => '0');
   signal CLK : std_logic := '0';
   signal CLR : std_logic := '0';
   signal BANDERAS : std_logic_vector(3 downto 0) := (others => '0');
   signal LF : std_logic := '0';

 	--Outputs
   signal S : std_logic_vector(19 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: main PORT MAP (
          CODIGO_FUNCION => CODIGO_FUNCION,
          CODIGO_OPERACION => CODIGO_OPERACION,
          CLK => CLK,
          CLR => CLR,
          BANDERAS => BANDERAS,
          LF => LF,
          S => S
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
	
		-- RBAND(0) = C
		-- RBAND(1) = Z
		-- RBAND(2) = N
		-- RBAND(3) = OV
		-- OV N Z C
       
		 --INSTRUCCIONES TIPO R--
		 
       CODIGO_OPERACION <= "00000";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "0000";
		 CLR <= '1';
		 LF <= '0';
       wait for CLK_period;
		 
		 
		 CODIGO_OPERACION <= "00000";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "0000";
		 CLR <= '1';
		 LF <= '0';
       wait for CLK_period;
		
		 --ADD-
		 --CODIGO_OPERACION <= "00000";
		 --CODIGO_FUNCION <= "0000";
		 --BANDERAS <= "0000";
		 --CLR <= '0';
		 --LF <= '0';
       --wait for CLK_period;
		
		 --SUB--
		 --CODIGO_OPERACION <= "00000";
		 --CODIGO_FUNCION <= "0001";
		 --BANDERAS <= "0000";
		 --CLR <= '0';
		 --LF <= '0';
       --wait for CLK_period;
		
		 --AND--
		 --CODIGO_OPERACION <= "00000";
		 --CODIGO_FUNCION <= "0010";
		 --BANDERAS <= "0100";
		 --CLR <= '0';
		 --LF <= '1';
       --wait for CLK_period;
		 
		 --OR--
		 --CODIGO_OPERACION <= "00000";
		 --CODIGO_FUNCION <= "0011";
		 --BANDERAS <= "1100";
		 --CLR <= '0';
		 --LF <= '1';
       --wait for CLK_period;
		 
		 --XOR--
		 --CODIGO_OPERACION <= "00000";
		 --CODIGO_FUNCION <= "0100";
		 --BANDERAS <= "0011";
		 --CLR <= '0';
		 --LF <= '1';
       --wait for CLK_period;
		 
		 --NAND--
		 --CODIGO_OPERACION <= "00000";
		 --CODIGO_FUNCION <= "0101";
		 --BANDERAS <= "1000";
		 --CLR <= '0';
		 --LF <= '1';
       --wait for CLK_period;		 
		 
		 --NOR--
		 --CODIGO_OPERACION <= "00000";
		 --CODIGO_FUNCION <= "0110";
		 --BANDERAS <= "0001";
		 --CLR <= '0';
		 --LF <= '1';
       --wait for CLK_period;
		 
		 --XNOR--
		 --CODIGO_OPERACION <= "00000";
		 --CODIGO_FUNCION <= "0111";
		 --BANDERAS <= "0100";
		 --CLR <= '0';
		 --LF <= '1';
       --wait for CLK_period;
		 
		 --NOT--
		 --CODIGO_OPERACION <= "00000";
		 --CODIGO_FUNCION <= "1000";
		 --BANDERAS <= "0010";
		 --CLR <= '0';
		 --LF <= '1';
       --wait for CLK_period;
		 
		 --SLL--
		 --CODIGO_OPERACION <= "00000";
		 --CODIGO_FUNCION <= "1001";
		 --BANDERAS <= "0000";
		 --CLR <= '0';
		 --LF <= '0';
       --wait for CLK_period;
		 
		 --SRL--
		 --CODIGO_OPERACION <= "00000";
		 --CODIGO_FUNCION <= "1010";
		 --BANDERAS <= "0000";
		 --CLR <= '0';
		 --LF <= '0';
       --wait for CLK_period;
		 
		 
		 --INSTRUCCIONES TIPO I--
		 --LI--
		 CODIGO_OPERACION <= "00001";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "1111";
		 CLR <= '0';
		 LF <= '0';
       wait for CLK_period;
		 
		 --LWI--
		 CODIGO_OPERACION <= "00010";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "0000";
		 CLR <= '0';
		 LF <= '0';
       wait for CLK_period;
		 
		 --LW--
		 CODIGO_OPERACION <= "10111";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "1111";
		 CLR <= '0';
		 LF <= '1';
       wait for CLK_period;
		 
		 --SWI--
		 CODIGO_OPERACION <= "00011";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "0000";
		 CLR <= '0';
		 LF <= '0';
       wait for CLK_period;
		 
		 --SW--
		 CODIGO_OPERACION <= "00100";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "1111";
		 CLR <= '0';
		 LF <= '1';
       wait for CLK_period;
		 
		 --ADDI--
		 CODIGO_OPERACION <= "00101";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "1111";
		 CLR <= '0';
		 LF <= '1';
       wait for CLK_period;
		 
		 --SUBI--
		 CODIGO_OPERACION <= "00110";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "1111";
		 CLR <= '0';
		 LF <= '1';
       wait for CLK_period;
		 
		 --ANDI--
		 CODIGO_OPERACION <= "00111";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "0110";
		 CLR <= '0';
		 LF <= '1';
       wait for CLK_period;
		 
		 --ORI--
		 CODIGO_OPERACION <= "01000";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "0110";
		 CLR <= '0';
		 LF <= '1';
       wait for CLK_period;
		 
		 --XORI--
		 CODIGO_OPERACION <= "01001";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "0110";
		 CLR <= '0';
		 LF <= '1';
       wait for CLK_period;
		 
		 --NANDI--
		 CODIGO_OPERACION <= "01010";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "0110";
		 CLR <= '0';
		 LF <= '1';
       wait for CLK_period;
		 
		 --NORI--
		 CODIGO_OPERACION <= "01011";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "0110";
		 CLR <= '0';
		 LF <= '1';
       wait for CLK_period;
		 
		 --XNORI--
		 CODIGO_OPERACION <= "01100";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "0110";
		 CLR <= '0';
		 LF <= '1';
       wait for CLK_period;
		 
		 --INSTRUCCIONES DE SALTO--
		 --BEQI--
		 CODIGO_OPERACION <= "01101";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "1111";
		 CLR <= '0';
		 LF <= '1';
       wait for CLK_period;
		 
		 --BNEI--
		 CODIGO_OPERACION <= "01110";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "1111";
		 CLR <= '0';
		 LF <= '1';
       wait for CLK_period;
		 
		 --BLTI--
		 CODIGO_OPERACION <= "01111";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "1111";
		 CLR <= '0';
		 LF <= '1';
       wait for CLK_period;
		 
		 --BLETI--
		 CODIGO_OPERACION <= "10000";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "1111";
		 CLR <= '0';
		 LF <= '1';
       wait for CLK_period;
		 
		 --BGTI--
		 CODIGO_OPERACION <= "10001";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "1111";
		 CLR <= '0';
		 LF <= '1';
       wait for CLK_period;
		 
		 --BGETI--
		 CODIGO_OPERACION <= "10010";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "1111";
		 CLR <= '0';
		 LF <= '1';
       wait for CLK_period;
		 
		 --B--
		 CODIGO_OPERACION <= "10011";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "0000";
		 CLR <= '0';
		 LF <= '0';
       wait for CLK_period;

		--INSTRUCCIONES DE SUBRUTINAS--
		--CALL--
		 CODIGO_OPERACION <= "10100";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "0000";
		 CLR <= '0';
		 LF <= '0';
       wait for CLK_period;
		 
		 --RET--
		 CODIGO_OPERACION <= "10101";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "0000";
		 CLR <= '0';
		 LF <= '0';
       wait for CLK_period;
		 
		 --OTRAS--
		 --NOP--
		 CODIGO_OPERACION <= "10110";
		 CODIGO_FUNCION <= "0000";
		 BANDERAS <= "0000";
		 CLR <= '0';
		 LF <= '0';
       wait for CLK_period;
      -- insert stimulus here 

      wait;
   end process;

END;

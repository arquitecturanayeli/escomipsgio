library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FSM_CTRL is
    Port ( CLK 	 : in  STD_LOGIC;
           CLR 	 : in  STD_LOGIC;
           TIPOR : in  STD_LOGIC;
           BEQ 	 : in  STD_LOGIC;
           BNE 	 : in  STD_LOGIC;
           BLT 	 : in  STD_LOGIC;
           BLE 	 : in  STD_LOGIC;
           BGT 	 : in  STD_LOGIC;
           BGE 	 : in  STD_LOGIC;
           NA 	 : in  STD_LOGIC;
           EQ 	 : in  STD_LOGIC;
           NEQ 	 : in  STD_LOGIC;
           L 	 : in  STD_LOGIC;
           LE 	 : in  STD_LOGIC;
           G 	 : in  STD_LOGIC;
           GE 	 : in  STD_LOGIC;
           SM 	 : out  STD_LOGIC;
           SDOPC : out  STD_LOGIC);
end FSM_CTRL;

architecture PROGRAMA of FSM_CTRL is
TYPE ESTADOS IS (A);
SIGNAL EDO_ACT, EDO_SGTE : ESTADOS;
begin

	AUTOMATA : PROCESS( EDO_ACT,TIPOR,BEQ, BNE,BLT,BLE,BGT,BGE,NA,EQ,NEQ,L,LE,G,GE )
	BEGIN
		SM 	<= '0';
		SDOPC <= '0';
		CASE( EDO_ACT ) IS
			WHEN A => EDO_SGTE <= A;
				IF( TIPOR = '0' )THEN
					IF( BEQ = '1' )THEN
						IF( NA = '1' )THEN
							SM <= '1'; --S=MEM_OPC[0];
						ELSE
							IF( EQ = '1' )THEN
								SM 	<= '1';
								SDOPC <= '1';
							ELSE
								SM <= '1';
							END IF;
						END IF;
					ELSIF( BNE = '1' )THEN
						IF( NA = '1' )THEN
							SM <= '1';  --S=MEM_OPC[0];
						ELSE
							IF( NEQ = '1' )THEN
								SM 	<= '1';   --S=MEM_FUN[OP_CODE]
								SDOPC <= '1';
							ELSE
								SM <= '1';    --S=MEM_OPC[0];
							END IF;
						END IF;

					ELSIF( BLT = '1' )THEN
						IF( NA = '1' )THEN
							SM <= '1';  --S=MEM_OPC[0];
						ELSE
							IF( L = '1' )THEN
								SM 	<= '1';   --S=MEM_FUN[OP_CODE]
								SDOPC <= '1';
							ELSE
								SM <= '1';    --S=MEM_OPC[0];
							END IF;
						END IF;

					ELSIF( BLE = '1' )THEN
						IF( NA = '1' )THEN
							SM <= '1';  --S=MEM_OPC[0];
						ELSE
							IF( LE = '1' )THEN
								SM 	<= '1';   --S=MEM_FUN[OP_CODE]
								SDOPC <= '1';
							ELSE
								SM <= '1';    --S=MEM_OPC[0];
							END IF;
						END IF;					
					ELSIF( BGT = '1' )THEN
						IF( NA = '1' )THEN
							SM <= '1';  --S=MEM_OPC[0];
						ELSE
							IF( G = '1' )THEN
								SM 	<= '1';   --S=MEM_FUN[OP_CODE]
								SDOPC <= '1';
							ELSE
								SM <= '1';    --S=MEM_OPC[0];
							END IF;
						END IF;					
					ELSIF( BGE = '1' )THEN
						IF( NA = '1' )THEN
							SM <= '1';  --S=MEM_OPC[0];
						ELSE
							IF( GE = '1' )THEN
								SM 	<= '1';   --S=MEM_FUN[OP_CODE]
								SDOPC <= '1';
							ELSE
								SM <= '1';    --S=MEM_OPC[0];
							END IF;
						END IF;
					
					ELSE
						SM 	<= '1'; --S=MEM_FUN[FUNCION_CODE]
						SDOPC <= '1';
					END IF;
				END IF;
		END CASE;
	END PROCESS AUTOMATA;

	TRANSICION : PROCESS( CLK, CLR )
	BEGIN
		IF( CLR = '1' )THEN
			EDO_ACT <= A;
		ELSIF( RISING_EDGE(CLK) )THEN
			EDO_ACT <= EDO_SGTE;
		END IF;
	END PROCESS TRANSICION;


end PROGRAMA;
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package pack_escomips is

component ALU is
	 GENERIC(N: INTEGER := 16 );
    PORT(A,B: IN  STD_LOGIC_VECTOR (N-1 DOWNTO 0);         
          OP: IN  STD_LOGIC_VECTOR (1 DOWNTO 0);
	  AINVERT: IN  STD_LOGIC;
	  BINVERT: IN  STD_LOGIC;
			  S: INOUT STD_LOGIC_VECTOR (N-1 DOWNTO 0);
			 OV: OUT STD_LOGIC;
			  Z: OUT STD_LOGIC;
			NEG: OUT STD_LOGIC;
          CO: INOUT STD_LOGIC);
end component;

component EXTDIR is
    Port ( A : in  STD_LOGIC_VECTOR (11 downto 0);
           B : out  STD_LOGIC_VECTOR (15 downto 0));
end component;

component EXTSIG is
    Port ( A : in  STD_LOGIC_VECTOR (11 downto 0);
           B : out  STD_LOGIC_VECTOR (15 downto 0));
end component;

component FILE_REGISTER is
	GENERIC(
	NBITS_ADDR:INTEGER :=4;
	NBITS_DATA: INTEGER:=16
	);

    Port ( READ_REGISTER1 	: in  STD_LOGIC_VECTOR (NBITS_ADDR-1 downto 0);
           READ_REGISTER2 	: in  STD_LOGIC_VECTOR (NBITS_ADDR-1 downto 0);
           SHAMT 				: in  STD_LOGIC_VECTOR (NBITS_ADDR-1 downto 0);
           WRITE_REGISTER 	: in  STD_LOGIC_VECTOR (NBITS_ADDR-1 downto 0);
           WRITE_DATA 		: in  STD_LOGIC_VECTOR (NBITS_DATA-1 downto 0);
			  CLK					: in  STD_LOGIC;
           WR 					: in  STD_LOGIC;
           SHE 				: in  STD_LOGIC;
           DIR 				: in  STD_LOGIC;
           READ_DATA1 		: inout STD_LOGIC_VECTOR (NBITS_DATA-1 downto 0);
           READ_DATA2 		: out STD_LOGIC_VECTOR (NBITS_DATA-1 downto 0));
end component;

component MD IS
	GENERIC(
	NBITS_ADDR:INTEGER :=16;
	NBITS_DATA: INTEGER:=16
	);

	PORT(
	CLK: in std_logic;
	WD: in STD_LOGIC;
	ADDR: in STD_LOGIC_VECTOR(NBITS_ADDR -1 downto 0);
	DIN: in STD_LOGIC_VECTOR(NBITS_DATA-1 downto 0);
	DOUT: out STD_LOGIC_VECTOR(NBITS_DATA-1 downto 0)
	);
END component;

component MP IS
	GENERIC(BITS_A: INTEGER := 16;
			  BITS_D: INTEGER := 25);
	PORT(A:  IN STD_LOGIC_VECTOR(BITS_A-1 DOWNTO 0);
		  D: OUT STD_LOGIC_VECTOR(BITS_D-1 DOWNTO 0));		  
END component;

component PILAHW is
    Port ( CLK : in  STD_LOGIC;
           CLR : in  STD_LOGIC;
           UP  : in  STD_LOGIC;
           DW  : in  STD_LOGIC;
           WPC : in  STD_LOGIC;
			  --SP  : out INTEGER RANGE 0 TO 15;
           D   : in  STD_LOGIC_VECTOR (15 downto 0);
           Q   : out STD_LOGIC_VECTOR (15 downto 0));
end component;

component UNIDADCONTROL is
    Port ( CODIGO_FUNCION   : in  STD_LOGIC_VECTOR (3 downto 0);
           CODIGO_OPERACION : in  STD_LOGIC_VECTOR (4 downto 0);
           CLK 				 : in  STD_LOGIC;
           CLR 				 : in  STD_LOGIC;
           BANDERAS         : in  STD_LOGIC_VECTOR (3 downto 0);
           LF               : in  STD_LOGIC;
			  S					 : out STD_LOGIC_VECTOR(19 DOWNTO 0)
			  );
end component;


end pack_escomips;


library IEEE;
use IEEE.STD_LOGIC_1164.all;

package Pack is

component CONDICION is
    Port ( RBAND : in  STD_LOGIC_VECTOR (3 downto 0);
           EQ : out  STD_LOGIC;
           NEQ : out  STD_LOGIC;
           L : out  STD_LOGIC;
           LE : out  STD_LOGIC;
           G : out  STD_LOGIC;
           GE : out  STD_LOGIC);
end component;

component DECO_INST is
    Port ( OP_CODE : in  STD_LOGIC_VECTOR (4 downto 0);
           TIPOR : out  STD_LOGIC;
           BEQ : out  STD_LOGIC;
           BNE : out  STD_LOGIC;
           BLT : out  STD_LOGIC;
           BLE : out  STD_LOGIC;
           BGT : out  STD_LOGIC;
           BGE : out  STD_LOGIC);
end component;

component DetectorNivel is
    Port ( CLK : in  STD_LOGIC;
           CLR : in  STD_LOGIC;
           NA : out  STD_LOGIC);
end component;

component FSM_CTRL is
    Port ( CLK : in  STD_LOGIC;
           CLR : in  STD_LOGIC;
           TIPOR : in  STD_LOGIC;
           BEQ : in  STD_LOGIC;
           BNE : in  STD_LOGIC;
           BLT : in  STD_LOGIC;
           BLE : in  STD_LOGIC;
           BGT : in  STD_LOGIC;
           BGE : in  STD_LOGIC;
           NA : in  STD_LOGIC;
           EQ : in  STD_LOGIC;
           NEQ : in  STD_LOGIC;
           L : in  STD_LOGIC;
           LE : in  STD_LOGIC;
           G : in  STD_LOGIC;
           GE : in  STD_LOGIC;
           SM : out  STD_LOGIC;
           SDOPC : out  STD_LOGIC);
end component;

component MF is
	  generic(
		INTA: INTEGER := 4;
		INTD: INTEGER := 20
	 );
	 
    Port ( AF : in  STD_LOGIC_VECTOR (INTA-1 downto 0);
           DF : out  STD_LOGIC_VECTOR (INTD-1 downto 0)
			 );
end component;

component MO is
	 generic(
		INTA: INTEGER := 5;
		INTD: INTEGER := 20
	 );
	 
    Port ( A : in  STD_LOGIC_VECTOR (INTA-1 downto 0);
           D : out STD_LOGIC_VECTOR (INTD-1 downto 0)
			 );
end component;

component muxSD is
    Port ( cp_code : in  STD_LOGIC_VECTOR (4 downto 0);
           sal     : out STD_LOGIC_VECTOR (4 downto 0);
			  sdopc   : in STD_LOGIC
			);
end component;

component muxSM is
	PORT(
		D : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
		DF: IN STD_LOGIC_VECTOR(19 DOWNTO 0);
		SM: IN STD_LOGIC;
		S : OUT STD_LOGIC_VECTOR(19 DOWNTO 0)
	);
end component;

component RegistroEstados is
    Port ( CLK : 				in  STD_LOGIC;
           CLR : 				in  STD_LOGIC;
           LF  : 				in  STD_LOGIC;
           BANDERAS_IN  :  in  STD_LOGIC_VECTOR (3 downto 0);
           BANDERAS_OUT :  out  STD_LOGIC_VECTOR (3 downto 0));
end component;

end Pack;


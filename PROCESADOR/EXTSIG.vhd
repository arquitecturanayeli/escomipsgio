library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity EXTSIG is
    Port ( A : in  STD_LOGIC_VECTOR (11 downto 0);
           B : out  STD_LOGIC_VECTOR (15 downto 0));
end EXTSIG;

architecture Behavioral of EXTSIG is

CONSTANT CEROS : STD_LOGIC_VECTOR(3 DOWNTO 0) := "0000";
CONSTANT UNOS  : STD_LOGIC_VECTOR(3 DOWNTO 0) := "1111";

begin

--	B <= UNOS&A WHEN A(11)='1' ELSE
--		  CEROS&A WHEN A(11)='0';

	PROCESS(A)
	BEGIN
		IF(A(11)='1')THEN
			B <= UNOS&A;
		ELSE
			B <= CEROS&A;
		END IF;
	END PROCESS;

end Behavioral;


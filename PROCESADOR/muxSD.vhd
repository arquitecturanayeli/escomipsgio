library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity muxSD is
    Port ( cp_code : in  STD_LOGIC_VECTOR (4 downto 0);
           sal     : out STD_LOGIC_VECTOR (4 downto 0);
			  sdopc   : in STD_LOGIC
			);
end muxSD;

architecture Behavioral of muxSD is

begin

		sal <= cp_code when sdopc = '1' else "00000" when sdopc = '0';

end Behavioral;


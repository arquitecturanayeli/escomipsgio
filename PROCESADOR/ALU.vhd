library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ALU is
	 GENERIC(N: INTEGER := 16 );
    PORT(A,B: IN  STD_LOGIC_VECTOR (N-1 DOWNTO 0);         
          OP: IN  STD_LOGIC_VECTOR (1 DOWNTO 0);
	  AINVERT: IN  STD_LOGIC;
	  BINVERT: IN  STD_LOGIC;
			  S: INOUT STD_LOGIC_VECTOR (N-1 DOWNTO 0);
			 OV: OUT STD_LOGIC;
			  Z: OUT STD_LOGIC;
			NEG: OUT STD_LOGIC;
          CO: INOUT STD_LOGIC);
end ALU;


architecture PROGRAMA of ALU is
begin	
	PALU: PROCESS( A, B, OP, AINVERT, BINVERT )
		VARIABLE EB   : STD_LOGIC_VECTOR(N-1 DOWNTO 0);
		VARIABLE C    : STD_LOGIC_VECTOR(N DOWNTO 0);
		VARIABLE I    : STD_LOGIC;
		VARIABLE MUXA : STD_LOGIC_VECTOR(N-1 DOWNTO 0) := (OTHERS=>'0');
		VARIABLE MUXB : STD_LOGIC_VECTOR(N-1 DOWNTO 0) := (OTHERS=>'0');
	
	BEGIN		
		C := (OTHERS => '0' );
		c(0) := BINVERT;
					
				FOR I IN 0 TO N-1 LOOP  
					MUXA(I):= A(I) XOR AINVERT;
					MUXB(I):= B(i) XOR BINVERT;
					CASE OP IS
						WHEN "00" =>
							S(I) <= MUXA(I) AND MUXB(I);
						WHEN "01" =>
							S(I) <= MUXA(I) OR MUXB(I);
						WHEN "10" =>
							S(I) <= MUXA(I) XOR MUXB(I);
						WHEN OTHERS =>	
							EB(I) := B(I) XOR BINVERT;
							S(I) 	<= A(I) XOR EB(I) XOR C(I);
							C(I+1):= (A(I) AND EB(I)) OR (A(I) AND C(I)) OR (EB(I) AND C(I));
						END CASE;					
				END LOOP;
				CO <= C(N);
				OV <= C(N) XOR C(N-1);	
	END PROCESS;
	Z <= NOT(S(0) OR S(1) OR S(2) OR S(3));
	NEG <= S(3);
end PROGRAMA;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity EXTDIR is
    Port ( A : in  STD_LOGIC_VECTOR (11 downto 0);
           B : out  STD_LOGIC_VECTOR (15 downto 0));
end EXTDIR;

architecture Behavioral of EXTDIR is

begin
	PROCESS(A)
	BEGIN
			B <= "0000"&A;
	END PROCESS;

end Behavioral;


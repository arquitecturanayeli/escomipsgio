/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "G:/PROCESADOR/PILAHW.vhd";
extern char *IEEE_P_3499444699;

char *ieee_p_3499444699_sub_4232465333_3536714472(char *, char *, char *, char *, int );


static void work_a_2958461521_3212880686_p_0(char *t0)
{
    char t27[16];
    char t28[16];
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    char *t8;
    char *t9;
    unsigned char t10;
    unsigned char t11;
    unsigned char t12;
    unsigned char t13;
    unsigned char t14;
    unsigned char t15;
    unsigned char t16;
    unsigned char t17;
    char *t18;
    unsigned char t19;
    unsigned char t20;
    char *t21;
    char *t22;
    char *t23;
    int t24;
    int t25;
    unsigned int t26;
    int t29;
    unsigned int t30;
    int t31;
    int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    char *t37;
    char *t38;
    char *t39;

LAB0:    xsi_set_current_line(28, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 992U);
    t4 = xsi_signal_has_event(t1);
    if (t4 == 1)
        goto LAB9;

LAB10:    t3 = (unsigned char)0;

LAB11:    if (t3 != 0)
        goto LAB7;

LAB8:
LAB3:    xsi_set_current_line(44, ng0);
    t1 = (t0 + 2288U);
    t2 = *((char **)t1);
    t1 = (t0 + 2408U);
    t5 = *((char **)t1);
    t24 = *((int *)t5);
    t25 = (t24 - 0);
    t6 = (t25 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t24);
    t7 = (16U * t6);
    t26 = (0 + t7);
    t1 = (t2 + t26);
    t8 = (t0 + 3792);
    t9 = (t8 + 56U);
    t18 = *((char **)t9);
    t21 = (t18 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t1, 16U);
    xsi_driver_first_trans_fast_port(t8);
    t1 = (t0 + 3712);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(29, ng0);
    t1 = (t0 + 2408U);
    t5 = *((char **)t1);
    t1 = (t5 + 0);
    *((int *)t1) = 0;
    xsi_set_current_line(30, ng0);
    t1 = xsi_get_transient_memory(256U);
    memset(t1, 0, 256U);
    t2 = t1;
    t6 = (16U * 1U);
    t5 = t2;
    memset(t5, (unsigned char)2, t6);
    t3 = (t6 != 0);
    if (t3 == 1)
        goto LAB5;

LAB6:    t8 = (t0 + 2288U);
    t9 = *((char **)t8);
    t8 = (t9 + 0);
    memcpy(t8, t1, 256U);
    goto LAB3;

LAB5:    t7 = (256U / t6);
    xsi_mem_set_data(t2, t2, t6, t7);
    goto LAB6;

LAB7:    xsi_set_current_line(32, ng0);
    t2 = (t0 + 1672U);
    t8 = *((char **)t2);
    t14 = *((unsigned char *)t8);
    t15 = (t14 == (unsigned char)3);
    if (t15 == 1)
        goto LAB18;

LAB19:    t13 = (unsigned char)0;

LAB20:    if (t13 == 1)
        goto LAB15;

LAB16:    t12 = (unsigned char)0;

LAB17:    if (t12 != 0)
        goto LAB12;

LAB14:    t1 = (t0 + 1672U);
    t2 = *((char **)t1);
    t10 = *((unsigned char *)t2);
    t11 = (t10 == (unsigned char)3);
    if (t11 == 1)
        goto LAB26;

LAB27:    t4 = (unsigned char)0;

LAB28:    if (t4 == 1)
        goto LAB23;

LAB24:    t3 = (unsigned char)0;

LAB25:    if (t3 != 0)
        goto LAB21;

LAB22:    t1 = (t0 + 1672U);
    t2 = *((char **)t1);
    t10 = *((unsigned char *)t2);
    t11 = (t10 == (unsigned char)2);
    if (t11 == 1)
        goto LAB34;

LAB35:    t4 = (unsigned char)0;

LAB36:    if (t4 == 1)
        goto LAB31;

LAB32:    t3 = (unsigned char)0;

LAB33:    if (t3 != 0)
        goto LAB29;

LAB30:    t1 = (t0 + 1672U);
    t2 = *((char **)t1);
    t10 = *((unsigned char *)t2);
    t11 = (t10 == (unsigned char)2);
    if (t11 == 1)
        goto LAB42;

LAB43:    t4 = (unsigned char)0;

LAB44:    if (t4 == 1)
        goto LAB39;

LAB40:    t3 = (unsigned char)0;

LAB41:    if (t3 != 0)
        goto LAB37;

LAB38:
LAB13:    goto LAB3;

LAB9:    t2 = (t0 + 1032U);
    t5 = *((char **)t2);
    t10 = *((unsigned char *)t5);
    t11 = (t10 == (unsigned char)3);
    t3 = t11;
    goto LAB11;

LAB12:    xsi_set_current_line(33, ng0);
    t2 = (t0 + 1832U);
    t21 = *((char **)t2);
    t2 = (t0 + 2288U);
    t22 = *((char **)t2);
    t2 = (t0 + 2408U);
    t23 = *((char **)t2);
    t24 = *((int *)t23);
    t25 = (t24 - 0);
    t6 = (t25 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t24);
    t7 = (16U * t6);
    t26 = (0 + t7);
    t2 = (t22 + t26);
    memcpy(t2, t21, 16U);
    goto LAB13;

LAB15:    t2 = (t0 + 1512U);
    t18 = *((char **)t2);
    t19 = *((unsigned char *)t18);
    t20 = (t19 == (unsigned char)2);
    t12 = t20;
    goto LAB17;

LAB18:    t2 = (t0 + 1352U);
    t9 = *((char **)t2);
    t16 = *((unsigned char *)t9);
    t17 = (t16 == (unsigned char)2);
    t13 = t17;
    goto LAB20;

LAB21:    xsi_set_current_line(35, ng0);
    t1 = (t0 + 2408U);
    t9 = *((char **)t1);
    t24 = *((int *)t9);
    t25 = (t24 + 1);
    t1 = (t0 + 2408U);
    t18 = *((char **)t1);
    t1 = (t18 + 0);
    *((int *)t1) = t25;
    xsi_set_current_line(36, ng0);
    t1 = (t0 + 1832U);
    t2 = *((char **)t1);
    t1 = (t0 + 2288U);
    t5 = *((char **)t1);
    t1 = (t0 + 2408U);
    t8 = *((char **)t1);
    t24 = *((int *)t8);
    t25 = (t24 - 0);
    t6 = (t25 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t24);
    t7 = (16U * t6);
    t26 = (0 + t7);
    t1 = (t5 + t26);
    memcpy(t1, t2, 16U);
    goto LAB13;

LAB23:    t1 = (t0 + 1512U);
    t8 = *((char **)t1);
    t14 = *((unsigned char *)t8);
    t15 = (t14 == (unsigned char)2);
    t3 = t15;
    goto LAB25;

LAB26:    t1 = (t0 + 1352U);
    t5 = *((char **)t1);
    t12 = *((unsigned char *)t5);
    t13 = (t12 == (unsigned char)3);
    t4 = t13;
    goto LAB28;

LAB29:    xsi_set_current_line(38, ng0);
    t1 = (t0 + 2408U);
    t9 = *((char **)t1);
    t24 = *((int *)t9);
    t25 = (t24 - 1);
    t1 = (t0 + 2408U);
    t18 = *((char **)t1);
    t1 = (t18 + 0);
    *((int *)t1) = t25;
    xsi_set_current_line(39, ng0);
    t1 = (t0 + 2288U);
    t2 = *((char **)t1);
    t1 = (t0 + 2408U);
    t5 = *((char **)t1);
    t24 = *((int *)t5);
    t25 = (t24 - 0);
    t6 = (t25 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t24);
    t7 = (16U * t6);
    t26 = (0 + t7);
    t1 = (t2 + t26);
    t8 = (t28 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 15;
    t9 = (t8 + 4U);
    *((int *)t9) = 0;
    t9 = (t8 + 8U);
    *((int *)t9) = -1;
    t29 = (0 - 15);
    t30 = (t29 * -1);
    t30 = (t30 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t30;
    t9 = ieee_p_3499444699_sub_4232465333_3536714472(IEEE_P_3499444699, t27, t1, t28, 1);
    t18 = (t0 + 2288U);
    t21 = *((char **)t18);
    t18 = (t0 + 2408U);
    t22 = *((char **)t18);
    t31 = *((int *)t22);
    t32 = (t31 - 0);
    t30 = (t32 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t31);
    t33 = (16U * t30);
    t34 = (0 + t33);
    t18 = (t21 + t34);
    t23 = (t27 + 12U);
    t35 = *((unsigned int *)t23);
    t36 = (1U * t35);
    memcpy(t18, t9, t36);
    goto LAB13;

LAB31:    t1 = (t0 + 1512U);
    t8 = *((char **)t1);
    t14 = *((unsigned char *)t8);
    t15 = (t14 == (unsigned char)3);
    t3 = t15;
    goto LAB33;

LAB34:    t1 = (t0 + 1352U);
    t5 = *((char **)t1);
    t12 = *((unsigned char *)t5);
    t13 = (t12 == (unsigned char)2);
    t4 = t13;
    goto LAB36;

LAB37:    xsi_set_current_line(41, ng0);
    t1 = (t0 + 2288U);
    t9 = *((char **)t1);
    t1 = (t0 + 2408U);
    t18 = *((char **)t1);
    t24 = *((int *)t18);
    t25 = (t24 - 0);
    t6 = (t25 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t24);
    t7 = (16U * t6);
    t26 = (0 + t7);
    t1 = (t9 + t26);
    t21 = (t28 + 0U);
    t22 = (t21 + 0U);
    *((int *)t22) = 15;
    t22 = (t21 + 4U);
    *((int *)t22) = 0;
    t22 = (t21 + 8U);
    *((int *)t22) = -1;
    t29 = (0 - 15);
    t30 = (t29 * -1);
    t30 = (t30 + 1);
    t22 = (t21 + 12U);
    *((unsigned int *)t22) = t30;
    t22 = ieee_p_3499444699_sub_4232465333_3536714472(IEEE_P_3499444699, t27, t1, t28, 1);
    t23 = (t0 + 2288U);
    t37 = *((char **)t23);
    t23 = (t0 + 2408U);
    t38 = *((char **)t23);
    t31 = *((int *)t38);
    t32 = (t31 - 0);
    t30 = (t32 * 1);
    xsi_vhdl_check_range_of_index(0, 15, 1, t31);
    t33 = (16U * t30);
    t34 = (0 + t33);
    t23 = (t37 + t34);
    t39 = (t27 + 12U);
    t35 = *((unsigned int *)t39);
    t36 = (1U * t35);
    memcpy(t23, t22, t36);
    goto LAB13;

LAB39:    t1 = (t0 + 1512U);
    t8 = *((char **)t1);
    t14 = *((unsigned char *)t8);
    t15 = (t14 == (unsigned char)2);
    t3 = t15;
    goto LAB41;

LAB42:    t1 = (t0 + 1352U);
    t5 = *((char **)t1);
    t12 = *((unsigned char *)t5);
    t13 = (t12 == (unsigned char)2);
    t4 = t13;
    goto LAB44;

}


extern void work_a_2958461521_3212880686_init()
{
	static char *pe[] = {(void *)work_a_2958461521_3212880686_p_0};
	xsi_register_didat("work_a_2958461521_3212880686", "isim/ESCOMIPS_TB_isim_beh.exe.sim/work/a_2958461521_3212880686.didat");
	xsi_register_executes(pe);
}

/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "G:/PROCESADOR/FILE_REGISTER.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_3620187407;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
int ieee_p_3620187407_sub_514432868_3965413181(char *, char *, char *);


static void work_a_1350576573_3212880686_p_0(char *t0)
{
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    unsigned char t5;
    unsigned char t6;
    char *t7;
    unsigned char t8;
    unsigned char t9;
    char *t10;
    char *t11;
    int t12;
    int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;

LAB0:    xsi_set_current_line(35, ng0);
    t1 = (t0 + 1792U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 5656);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(36, ng0);
    t3 = (t0 + 1992U);
    t4 = *((char **)t3);
    t5 = *((unsigned char *)t4);
    t6 = (t5 == (unsigned char)3);
    if (t6 != 0)
        goto LAB5;

LAB7:
LAB6:    goto LAB3;

LAB5:    xsi_set_current_line(37, ng0);
    t3 = (t0 + 2152U);
    t7 = *((char **)t3);
    t8 = *((unsigned char *)t7);
    t9 = (t8 == (unsigned char)2);
    if (t9 != 0)
        goto LAB8;

LAB10:    t1 = (t0 + 2152U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t5 = (t2 == (unsigned char)3);
    if (t5 != 0)
        goto LAB11;

LAB12:
LAB9:    goto LAB6;

LAB8:    xsi_set_current_line(38, ng0);
    t3 = (t0 + 1672U);
    t10 = *((char **)t3);
    t3 = (t0 + 1512U);
    t11 = *((char **)t3);
    t3 = (t0 + 8688U);
    t12 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t11, t3);
    t13 = (t12 - 15);
    t14 = (t13 * -1);
    t15 = (16U * t14);
    t16 = (0U + t15);
    t17 = (t0 + 5784);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    t20 = (t19 + 56U);
    t21 = *((char **)t20);
    memcpy(t21, t10, 16U);
    xsi_driver_first_trans_delta(t17, t16, 16U, 0LL);
    goto LAB9;

LAB11:    xsi_set_current_line(40, ng0);
    t1 = (t0 + 2952U);
    t4 = *((char **)t1);
    t1 = (t0 + 1512U);
    t7 = *((char **)t1);
    t1 = (t0 + 8688U);
    t12 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t7, t1);
    t13 = (t12 - 15);
    t14 = (t13 * -1);
    t15 = (16U * t14);
    t16 = (0U + t15);
    t10 = (t0 + 5784);
    t11 = (t10 + 56U);
    t17 = *((char **)t11);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    memcpy(t19, t4, 16U);
    xsi_driver_first_trans_delta(t10, t16, 16U, 0LL);
    goto LAB9;

}

static void work_a_1350576573_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    int t6;
    char *t7;
    unsigned char t8;
    unsigned char t9;
    int t10;
    char *t11;
    int t12;
    int t13;
    char *t14;
    char *t15;
    int t16;
    int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    unsigned char t22;
    unsigned char t23;
    char *t24;
    char *t25;
    int t26;
    int t27;
    int t28;
    char *t29;
    char *t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;

LAB0:    xsi_set_current_line(53, ng0);
    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t1 = (t0 + 3488U);
    t3 = *((char **)t1);
    t1 = (t3 + 0);
    memcpy(t1, t2, 16U);
    xsi_set_current_line(54, ng0);
    t4 = (4 - 1);
    t1 = (t0 + 9160);
    *((int *)t1) = 0;
    t2 = (t0 + 9164);
    *((int *)t2) = t4;
    t5 = 0;
    t6 = t4;

LAB2:    if (t5 <= t6)
        goto LAB3;

LAB5:    t1 = (t0 + 5672);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(56, ng0);
    t3 = (t0 + 2312U);
    t7 = *((char **)t3);
    t8 = *((unsigned char *)t7);
    t9 = (t8 == (unsigned char)2);
    if (t9 != 0)
        goto LAB6;

LAB8:    t1 = (t0 + 2312U);
    t2 = *((char **)t1);
    t8 = *((unsigned char *)t2);
    t9 = (t8 == (unsigned char)3);
    if (t9 != 0)
        goto LAB20;

LAB21:
LAB7:
LAB4:    t1 = (t0 + 9160);
    t5 = *((int *)t1);
    t2 = (t0 + 9164);
    t6 = *((int *)t2);
    if (t5 == t6)
        goto LAB5;

LAB33:    t4 = (t5 + 1);
    t5 = t4;
    t3 = (t0 + 9160);
    *((int *)t3) = t5;
    goto LAB2;

LAB6:    xsi_set_current_line(57, ng0);
    t10 = (16 - 1);
    t3 = (t0 + 9168);
    *((int *)t3) = t10;
    t11 = (t0 + 9172);
    *((int *)t11) = 0;
    t12 = t10;
    t13 = 0;

LAB9:    if (t12 >= t13)
        goto LAB10;

LAB12:    xsi_set_current_line(67, ng0);
    t1 = (t0 + 3488U);
    t2 = *((char **)t1);
    t1 = (t0 + 5848);
    t3 = (t1 + 56U);
    t7 = *((char **)t3);
    t11 = (t7 + 56U);
    t14 = *((char **)t11);
    memcpy(t14, t2, 16U);
    xsi_driver_first_trans_fast(t1);
    goto LAB7;

LAB10:    xsi_set_current_line(58, ng0);
    t14 = (t0 + 1352U);
    t15 = *((char **)t14);
    t14 = (t0 + 9160);
    t16 = *((int *)t14);
    t17 = (t16 - 3);
    t18 = (t17 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t14));
    t19 = (1U * t18);
    t20 = (0 + t19);
    t21 = (t15 + t20);
    t22 = *((unsigned char *)t21);
    t23 = (t22 == (unsigned char)3);
    if (t23 != 0)
        goto LAB13;

LAB15:
LAB14:
LAB11:    t1 = (t0 + 9168);
    t12 = *((int *)t1);
    t2 = (t0 + 9172);
    t13 = *((int *)t2);
    if (t12 == t13)
        goto LAB12;

LAB19:    t4 = (t12 + -1);
    t12 = t4;
    t3 = (t0 + 9168);
    *((int *)t3) = t12;
    goto LAB9;

LAB13:    xsi_set_current_line(59, ng0);
    t24 = (t0 + 9168);
    t25 = (t0 + 9160);
    t26 = xsi_vhdl_pow(2, *((int *)t25));
    t27 = *((int *)t24);
    t28 = (t27 - t26);
    t29 = (t0 + 3608U);
    t30 = *((char **)t29);
    t29 = (t30 + 0);
    *((int *)t29) = t28;
    xsi_set_current_line(60, ng0);
    t1 = (t0 + 3608U);
    t2 = *((char **)t1);
    t4 = *((int *)t2);
    t8 = (t4 < 0);
    if (t8 != 0)
        goto LAB16;

LAB18:    xsi_set_current_line(63, ng0);
    t1 = (t0 + 3488U);
    t2 = *((char **)t1);
    t1 = (t0 + 3608U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t10 = (t4 - 15);
    t18 = (t10 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, t4);
    t19 = (1U * t18);
    t20 = (0 + t19);
    t1 = (t2 + t20);
    t8 = *((unsigned char *)t1);
    t7 = (t0 + 3488U);
    t11 = *((char **)t7);
    t7 = (t0 + 9168);
    t16 = *((int *)t7);
    t17 = (t16 - 15);
    t31 = (t17 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t7));
    t32 = (1U * t31);
    t33 = (0 + t32);
    t14 = (t11 + t33);
    *((unsigned char *)t14) = t8;

LAB17:    goto LAB14;

LAB16:    xsi_set_current_line(61, ng0);
    t1 = (t0 + 3488U);
    t3 = *((char **)t1);
    t1 = (t0 + 9168);
    t10 = *((int *)t1);
    t16 = (t10 - 15);
    t18 = (t16 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t1));
    t19 = (1U * t18);
    t20 = (0 + t19);
    t7 = (t3 + t20);
    *((unsigned char *)t7) = (unsigned char)2;
    goto LAB17;

LAB20:    xsi_set_current_line(71, ng0);
    t4 = (16 - 1);
    t1 = (t0 + 9176);
    *((int *)t1) = 0;
    t3 = (t0 + 9180);
    *((int *)t3) = t4;
    t10 = 0;
    t12 = t4;

LAB22:    if (t10 <= t12)
        goto LAB23;

LAB25:    xsi_set_current_line(81, ng0);
    t1 = (t0 + 3488U);
    t2 = *((char **)t1);
    t1 = (t0 + 5848);
    t3 = (t1 + 56U);
    t7 = *((char **)t3);
    t11 = (t7 + 56U);
    t14 = *((char **)t11);
    memcpy(t14, t2, 16U);
    xsi_driver_first_trans_fast(t1);
    goto LAB7;

LAB23:    xsi_set_current_line(72, ng0);
    t7 = (t0 + 1352U);
    t11 = *((char **)t7);
    t7 = (t0 + 9160);
    t13 = *((int *)t7);
    t16 = (t13 - 3);
    t18 = (t16 * -1);
    xsi_vhdl_check_range_of_index(3, 0, -1, *((int *)t7));
    t19 = (1U * t18);
    t20 = (0 + t19);
    t14 = (t11 + t20);
    t22 = *((unsigned char *)t14);
    t23 = (t22 == (unsigned char)3);
    if (t23 != 0)
        goto LAB26;

LAB28:
LAB27:
LAB24:    t1 = (t0 + 9176);
    t10 = *((int *)t1);
    t2 = (t0 + 9180);
    t12 = *((int *)t2);
    if (t10 == t12)
        goto LAB25;

LAB32:    t4 = (t10 + 1);
    t10 = t4;
    t3 = (t0 + 9176);
    *((int *)t3) = t10;
    goto LAB22;

LAB26:    xsi_set_current_line(73, ng0);
    t15 = (t0 + 9176);
    t21 = (t0 + 9160);
    t17 = xsi_vhdl_pow(2, *((int *)t21));
    t26 = *((int *)t15);
    t27 = (t26 + t17);
    t24 = (t0 + 3608U);
    t25 = *((char **)t24);
    t24 = (t25 + 0);
    *((int *)t24) = t27;
    xsi_set_current_line(74, ng0);
    t1 = (t0 + 3608U);
    t2 = *((char **)t1);
    t4 = *((int *)t2);
    t13 = (16 - 1);
    t8 = (t4 > t13);
    if (t8 != 0)
        goto LAB29;

LAB31:    xsi_set_current_line(77, ng0);
    t1 = (t0 + 3488U);
    t2 = *((char **)t1);
    t1 = (t0 + 3608U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t13 = (t4 - 15);
    t18 = (t13 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, t4);
    t19 = (1U * t18);
    t20 = (0 + t19);
    t1 = (t2 + t20);
    t8 = *((unsigned char *)t1);
    t7 = (t0 + 3488U);
    t11 = *((char **)t7);
    t7 = (t0 + 9176);
    t16 = *((int *)t7);
    t17 = (t16 - 15);
    t31 = (t17 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t7));
    t32 = (1U * t31);
    t33 = (0 + t32);
    t14 = (t11 + t33);
    *((unsigned char *)t14) = t8;

LAB30:    goto LAB27;

LAB29:    xsi_set_current_line(75, ng0);
    t1 = (t0 + 3488U);
    t3 = *((char **)t1);
    t1 = (t0 + 9176);
    t16 = *((int *)t1);
    t17 = (t16 - 15);
    t18 = (t17 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, *((int *)t1));
    t19 = (1U * t18);
    t20 = (0 + t19);
    t7 = (t3 + t20);
    *((unsigned char *)t7) = (unsigned char)2;
    goto LAB30;

}

static void work_a_1350576573_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    xsi_set_current_line(88, ng0);

LAB3:    t1 = (t0 + 2792U);
    t2 = *((char **)t1);
    t1 = (t0 + 1032U);
    t3 = *((char **)t1);
    t1 = (t0 + 8640U);
    t4 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t3, t1);
    t5 = (t4 - 15);
    t6 = (t5 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, t4);
    t7 = (16U * t6);
    t8 = (0 + t7);
    t9 = (t2 + t8);
    t10 = (t0 + 5912);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t9, 16U);
    xsi_driver_first_trans_fast_port(t10);

LAB2:    t15 = (t0 + 5688);
    *((int *)t15) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_1350576573_3212880686_p_3(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    xsi_set_current_line(89, ng0);

LAB3:    t1 = (t0 + 2792U);
    t2 = *((char **)t1);
    t1 = (t0 + 1192U);
    t3 = *((char **)t1);
    t1 = (t0 + 8656U);
    t4 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t3, t1);
    t5 = (t4 - 15);
    t6 = (t5 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, t4);
    t7 = (16U * t6);
    t8 = (0 + t7);
    t9 = (t2 + t8);
    t10 = (t0 + 5976);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t9, 16U);
    xsi_driver_first_trans_fast_port(t10);

LAB2:    t15 = (t0 + 5704);
    *((int *)t15) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_1350576573_3212880686_init()
{
	static char *pe[] = {(void *)work_a_1350576573_3212880686_p_0,(void *)work_a_1350576573_3212880686_p_1,(void *)work_a_1350576573_3212880686_p_2,(void *)work_a_1350576573_3212880686_p_3};
	xsi_register_didat("work_a_1350576573_3212880686", "isim/ESCOMIPS_TB_isim_beh.exe.sim/work/a_1350576573_3212880686.didat");
	xsi_register_executes(pe);
}

/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;

char *IEEE_P_2592010699;
char *WORK_P_0972559976;
char *STD_STANDARD;
char *IEEE_P_3620187407;
char *WORK_P_3137716868;
char *IEEE_P_3499444699;


int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    ieee_p_2592010699_init();
    work_p_3137716868_init();
    ieee_p_3499444699_init();
    ieee_p_3620187407_init();
    work_p_0972559976_init();
    work_a_3175194644_3023556817_init();
    work_a_3854614078_3212880686_init();
    work_a_2220517448_3212880686_init();
    work_a_1350576573_3212880686_init();
    work_a_0096494558_2906416551_init();
    work_a_3827261716_3023556817_init();
    work_a_2958461521_3212880686_init();
    work_a_0635695433_3023556817_init();
    work_a_2253722718_3023556817_init();
    work_a_2175728961_3023556817_init();
    work_a_1061504304_3212880686_init();
    work_a_1266646782_3212880686_init();
    work_a_2674701936_3212880686_init();
    work_a_2208504692_3212880686_init();
    work_a_0406736884_3212880686_init();
    work_a_3030251951_3212880686_init();
    work_a_2178422333_3212880686_init();
    work_a_0991361627_3212880686_init();
    work_a_3107243474_2372691052_init();


    xsi_register_tops("work_a_3107243474_2372691052");

    IEEE_P_2592010699 = xsi_get_engine_memory("ieee_p_2592010699");
    xsi_register_ieee_std_logic_1164(IEEE_P_2592010699);
    WORK_P_0972559976 = xsi_get_engine_memory("work_p_0972559976");
    STD_STANDARD = xsi_get_engine_memory("std_standard");
    IEEE_P_3620187407 = xsi_get_engine_memory("ieee_p_3620187407");
    WORK_P_3137716868 = xsi_get_engine_memory("work_p_3137716868");
    IEEE_P_3499444699 = xsi_get_engine_memory("ieee_p_3499444699");

    return xsi_run_simulation(argc, argv);

}

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity muxSM is
	PORT(
		D : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
		DF: IN STD_LOGIC_VECTOR(19 DOWNTO 0);
		SM: IN STD_LOGIC;
		S : OUT STD_LOGIC_VECTOR(19 DOWNTO 0)
	);
end muxSM;

architecture Behavioral of muxSM is
begin
 
 S <= DF WHEN SM='0' ELSE D WHEN SM='1';

end Behavioral;

